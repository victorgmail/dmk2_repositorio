#include <Arduino.h>
#include <RobotMove.h>

int pin=7;
int pulsomin=600;
int pulsomax=2450;

/* int speeds = 0; //delay between two steps, received from the computer
char buffer[16];
int step=0;
int pos=0;
int receivedCommand; //character for commands
bool newData=false;
int len=0;
int angleSteps=0; */


Robot barbot(pin, pulsomin, pulsomax, 33, 31);

void setup()
{
  barbot.init();
}

void loop()
{
  barbot.write(90, 60, 10); //angulo nema, angulo servo, velocidad
  delay(3000);
  barbot.write(180, 45, 10);
  delay(3000);
  barbot.write(270, 90, 10);
  delay(3000);
  barbot.write(0, 0, 10);
  delay(3000);
}
/*   if (Serial.available() > 0){ //if something comes
    receivedCommand = Serial.read(); // this will read the command character
    newData = true; //this creates a flag
    buffer[len++] = receivedCommand;  
    //
    // check for overflow
    //
    if (len >= 16)
    {
      // overflow, resetting
      len = 0;
    }
    if (newData == true){
      if(receivedCommand == '\n'){
        int n = sscanf(buffer, "%d %d %d", &angleSteps, &pos, &speeds);
        if (n == 3){    
          barbot.write(angleSteps, pos, speeds);
        }else{
          Serial.println("ERROR");
        }
        len = 0;
      }
    }
  } */


