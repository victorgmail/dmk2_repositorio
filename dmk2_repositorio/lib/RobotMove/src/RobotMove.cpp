#include <Arduino.h>
#include "RobotMove.h"

Robot::Robot(int pin, int pulsomin, int pulsomax, int dirPin, int pulPin)
{
    this->pin = pin;
    this->pulsomin = pulsomin;
    this->pulsomax = pulsomax;
    this->dirPin = dirPin;
    this->pulPin = pulPin;
}
void Robot::init()
{
    servo1.attach(pin, pulsomin, pulsomax);
    servo1.write(180 - i);
    pinMode(dirPin, OUTPUT);
    pinMode(pulPin, OUTPUT);
    Serial.begin(9600);

}

void Robot::write(int angleNema, int angleServo, int speeds)
{
    int step = map(angleNema, 0, 360, 0, 800);
    int pos = angleServo;
    if (step >= iint && pos >= jint)
    {
        if (((step - iint) > (pos - jint)) || (((step - iint) > (pos - jint)) && (pos-jint == 0)))
        {
            c = ((float)pos - j) / ((float)step - (float)i);
            while (roundf(i) < step)
            {
                Serial.print("condicional 1, ");
                i++;
                iint = (int)i;
                if (j < (float)pos)
                {
                    j = j + c;
                    jint = (int)roundf(j);
                    servo1.write(180 - jint);
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                digitalWrite(dirPin, 0); //
                digitalWrite(pulPin, HIGH);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                digitalWrite(pulPin, LOW);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
            }
        }
        else if (((step - iint) < (pos - jint)) || (((step - iint) < (pos - jint)) && (step-iint == 0)))
        {
            c = ((float)step - (float)i) / ((float)pos - j);
            while (roundf(j) < (float)pos)
            {
                Serial.print("condicional 2, ");
                j++;
                jint = (int)j;
                if (i < (float)step)
                {
                    i = i + c;
                    iint = (int)roundf(i);
                }
                if (iant != iint)
                {
                    servo1.write(180 - jint);
                    digitalWrite(dirPin, 0); //
                    digitalWrite(pulPin, HIGH);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    digitalWrite(pulPin, LOW);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                else
                {
                    servo1.write(180 - jint);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                iant = iint;
            }
        }
    }
    else if (step <= iint && pos <= jint)
    {
        if (((iint - step) > (jint - pos)) || (((iint - step) > (jint - pos)) && (jint - pos == 0)))
        {
            c = (j - (float)pos) / (i - (float)step);
            while (roundf(i) > (float)step)
            {

                i--;
                iint = (int)roundf(i);
                if (j > (float)pos)
                {
                    j = j - c;
                    jint = (int)roundf(j);
                    servo1.write(180 - jint);
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                digitalWrite(dirPin, 1);
                digitalWrite(pulPin, HIGH);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                digitalWrite(pulPin, LOW);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
            }
        }
        else if (((iint - step) < (jint - pos)) || (((iint - step) < (jint - pos)) && (iint - step == 0)))
        {
            c = (i - (float)step) / (j - (float)pos);
            while (roundf(j) > (float)pos)
            {
                j--;
                jint = (int)round(j);
                if (i > (float)step)
                {
                    i = i - c;
                    iint = (int)roundf(i);
                }
                if (iant != iint)
                {
                    servo1.write(180 - jint);
                    digitalWrite(dirPin, 1);
                    digitalWrite(pulPin, HIGH);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    digitalWrite(pulPin, LOW);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                else
                {
                    servo1.write(180 - jint);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                iant = iint;
            }
        }
    }
    else if (step > iint && pos < jint)
    {
        if (((float)step - i) > (j - (float)pos))
        {
            c = (j - (float)pos) / ((float)step - i);
            while (roundf(i) < (float)step)
            {
                Serial.print("condicional 5,");
                i++;
                iint = (int)roundf(i);
                if (j > (float)pos)
                {
                    j = j - c;
                    jint = (int)roundf(j);
                    servo1.write(180 - jint);
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                digitalWrite(dirPin, 0);
                digitalWrite(pulPin, HIGH);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                digitalWrite(pulPin, LOW);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
            }
        }
        else if (((float)step - i) < (j - (float)pos))
        {
            c = ((float)step - i) / (j - (float)pos);
            while (roundf(j) > (float)pos)
            {
                Serial.print("condicional 6,");
                j--;
                jint = (int)roundf(j);
                if (i < (float)step)
                {
                    i = i + c;
                    iint = (int)roundf(i);
                }
                if (iant != iint)
                {
                    servo1.write(180 - jint);
                    digitalWrite(dirPin, 0);
                    digitalWrite(pulPin, HIGH);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    digitalWrite(pulPin, LOW);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                else
                {
                    servo1.write(180 - jint);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                iant = iint;
            }
        }
    }
    else if (step < iint && pos > jint)
    {
        if ((i - (float)step) > ((float)pos - j))
        {
            c = ((float)pos - j) / (i - (float)step);
            while (roundf(i) > (float)step)
            {
                i--;
                iint = (int)roundf(i);
                if (j < (float)pos)
                {
                    j = j + c;
                    jint = (int)roundf(j);
                    servo1.write(180 - jint);
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                digitalWrite(dirPin, 1);
                digitalWrite(pulPin, HIGH);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                digitalWrite(pulPin, LOW);
                delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
            }
        }
        else if ((i - (float)step) < ((float)pos - j))
        {
            c = (i - (float)step) / ((float)pos - j);
            while (roundf(j) < (float)pos)
            {
                j++;
                jint = (int)roundf(j);
                if (i > (float)step)
                {
                    i = i - c;
                    iint = (int)roundf(i);
                }
                if (iant != iint)
                {
                    servo1.write(180 - jint);
                    digitalWrite(dirPin, 1);
                    digitalWrite(pulPin, HIGH);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    digitalWrite(pulPin, LOW);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                else
                {
                    servo1.write(180 - jint);
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                    delayMicroseconds(map(speeds, 1, 2000, 2000, 1));
                }
                Serial.print(iint);
                Serial.print(",");
                Serial.println(jint);
                iant = iint;
            }
        }
    }
    else
    {
    }
}