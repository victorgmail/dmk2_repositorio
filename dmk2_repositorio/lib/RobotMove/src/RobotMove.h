#ifndef _ROBOT_H
#define _ROBOT_H

#include <Arduino.h>
#include <Servo.h>

class Robot
{
private:
    Servo servo1;
    int pin;
    int pulsomin;
    int pulsomax;
    int dirPin;
    int pulPin;
    float j = 0;
    int jint = 0;
    float i = 0;
    int iint = 0;
    float c = 0;
    int iant = 0;

public:
    Robot(int pin, int pulsomin, int pulsomax, int pinDir, int pinPul);
    void init();
    void write(int angleNema, int angleServo, int speeds);
};

#endif